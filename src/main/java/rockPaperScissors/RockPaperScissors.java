package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (true) {
            String humanChoice = userChoice();
            if (!humanChoice.equals("rock") && !humanChoice.equals("scissors") && (!humanChoice.equals("paper"))) {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            } else {
                roundCounter += 1;
                String cpChoice = randomChoice();
                boolean result = humanWinner(humanChoice, cpChoice);
                if (result) {
                    humanScore += 1;
                } else if (humanChoice.equals(cpChoice)){
                } else {
                    computerScore += 1;
                }
                System.out.println(printWinner(result, humanChoice, cpChoice, humanScore, computerScore));
                String answer = readInput("Do you wish to continue playing? (y/n)?");
                if (answer.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                } else if (!answer.equals("y")) {
                    System.out.println("I do not understand " + answer + ". Could you try again?");
                }
            }
        }
    }

    /**
     * Reads input from console with given prompt
     *
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String userChoice() {
        System.out.println("Let's play round " + roundCounter);
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        return humanChoice;
    }

    public String randomChoice() {
        int random = (int) (Math.random() * 3);
        if (random == 0) {
            return "rock";
        } else if (random == 1) {
            return "paper";
        }
        return "scissors";
    }

    public boolean humanWinner(String choice1, String choice2) {
        if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        } else if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else {
            return choice2.equals("paper");
        }
    }

    public String printWinner(boolean result, String humanChoice, String cpChoice, int humanScore, int computerScore) {
        String winner;
        if (result) {
            winner = ". Human wins!";
        } else if (humanChoice.equals(cpChoice)) {
            winner = ". It's a tie!";
        } else {
            winner = ". Computer wins!";
        }
        return "Human chose " + humanChoice + ", computer chose " + cpChoice + winner + "\nScore: human " + humanScore + ", computer " + computerScore;
    }

}